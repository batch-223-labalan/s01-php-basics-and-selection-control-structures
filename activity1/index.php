<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 1</title>
</head>
<body>
	<h2>Full Address</h2>
	<p><?php echo getFullAddress('3F Caswynn Bldg.', 'Timog Avenue', 'Quezon City', 'Philippines'); ?></p>
	<p><?php echo getFullAddress('3F Enzo Bldg.', 'Buendia Avenue', 'Makati City', 'Philippines'); ?></p>

	<h2>Letter-Based Grading</h2>
	<p><?php echo getLetterGrade(87); ?></p>
	<p><?php echo getLetterGrade(94); ?></p>
	<p><?php echo getLetterGrade(74); ?></p>

</body>
</html>