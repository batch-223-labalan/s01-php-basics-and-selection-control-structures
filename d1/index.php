<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S01: PHP Basics and Selection Control</title>
</head>
<body>
	<h1>Echoing Values</h1>
	<p><?php echo "Good day $name! Your given email is $email"; ?></p>

	<p><?php echo PI; ?></p>
	<p><?php echo 'The value of Pi is: ' . PI; ?></p>

	<h1>DATA TYPES</h1>
	<p><?php echo $hasTravelleddAbroad; ?></p>
	<p><?php echo $spouse; ?></p>

	<!-- DATA TYPE -->
	<p><?php echo gettype($hasTravelleddAbroad); ?></p>
	<p><?php echo gettype($spouse); ?></p>

	<!-- DATA TYPE VALUE -->
	<p><?php echo var_dump($hasTravelleddAbroad); ?></p>
	<p><?php echo var_dump($spouse); ?></p>



	<!-- OBJECTS -->
	<p><?php var_dump ($gradeObj); ?></p>
	<p><?php echo $gradeObj->firstGrading; ?></p>
	<p><?php echo $personObj->address->state; ?></p>

	<p><?php echo $grades[3]; ?></p>



	<!-- Operators -->
	<h1>Operators</h1>
	<p>X: <?php echo $x; ?></p>
	<p>Y: <?php echo $y; ?></p>

	<!-- Boolean -->
	<p>is Legal Age: <?php var_dump($isLegalAge); ?></p>
	<p>is Registerd: <?php var_dump($isRegisterd); ?></p>



	<!-- Arithmetic Operator -->
	<h2>Arithmetic Operators</h2>
	<p>Sum: <?php echo $x + $y; ?></p>
	<p>Difference: <?php echo $x - $y; ?></p>
	<p>Product: <?php echo $x * $y; ?></p>
	<p>Modulo: <?php echo $x % $y; ?></p>



	<!-- Equality Operator -->
	<h2>Equality operator</h2>
	<p>Loose Equality: <?php var_dump($x == '1342.14') ?></p>
	<p>Strict Equality: <?php var_dump($x === '1342.14') ?></p>
	<p>Loose Inequality: <?php var_dump($x != '1342.14') ?></p>
	<p>Strict Inequality: <?php var_dump($x !== '1342.14') ?></p>


	<!-- Greater or Equal operator -->
	<h2>Greate/Lesser operator</h2>
	<p>Is Lesser: <?php var_dump($x < $y); ?></p>
	<p>Is Greater: <?php var_dump($x > $y); ?></p>
	<p>Is Lesser or Equal: <?php var_dump($x <= $y); ?></p>
	<p>Is Greater or Equal: <?php var_dump($x >= $y); ?></p>


	<!-- Logical Operators -->
	<h2>Logical Operator</h2>
	<p>Are all requirements Met: <?php var_dump($isLegalAge && $isRegisterd); ?></p>
	<p>Are all requirements Met: <?php var_dump($isLegalAge || $isRegisterd); ?></p>
	<p>Are all requirements Met: <?php var_dump($isLegalAge && !$isRegisterd); ?></p>




	<!-- FUNCTIONS -->
	<h2>Function</h2>
	<p><?php echo getFullname('John', 'D', 'Smith'); ?></p>


	<!-- Selection Control Structures -->
	<h2>Selection Control Structures</h2>
	<h2>If-Elseif-Else</h2>
	<p><?php echo determineTyphoonIntensity(20) ?></p>

	<h2>Ternary Sample</h2>
	<p>78: <?php echo isUnderAge(78); ?></p>
	<p>17: <?php echo isUnderAge(17); ?></p>

	<h2>Switch Case</h2>
	<p><?php echo determineComputer(10); ?></p>


	<!-- Error handling -->
	<h2>Try-Catch-Finally</h2>
	<p><?php greeting("Hello"); ?></p>
	<p><?php greeting(25); ?></p>




</body>
</html>