<?php

// [SECTION] Variables
$name = 'John Smith';
$email = 'johnsmith@mail.com';


// CONSTANTS 
/*
>> Here in php, COnst are different from variables
>> Naming convention : CAPITAL
>> Doesn't use $

*/

define('PI', 3.1216);


// STRINGS
$state = 'New York';
$country = 'United States';
$address = $state .', '.$country;
$address = "$state, $country";

// INTEGER
$age = 31;
$headcount = 26;

//FLOATS
$grade = 98.2;
$distanceInKilometers = 1342.12;


//Boolean
$hasTravelleddAbroad = false;
$haveSymptoms = true;

//ARRAYS
$grades = array(98.7, 92.1, 90.2, 94.6);


//NULL
$spouse = null;
$middleName = null;


// Objects
$gradeObj = (object)[
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6
];

$personObj = (object)[
	'fullName' => 'John Smith',
	'isMarried' => false,
	'age' => 35,
	'address' => (object)[
		'state' => 'New York',
		'country' => 'United States of America'
	]
];


//OPERATORS

//Assignment Operators

$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegisterd = false;


//FUNCTIONS

function getFullName($firstName, $middleInitial, $lastName){
	return "$lastName, $firstName $middleInitial";
}


//SELECTION CONTROL STRUCTURES

function determineTyphoonIntensity($winsdSpeed){
	if($windSpeed <30 ){
		return 'Not a typhoon yet';
	} else if($windSpeed <= 61 ){
		return 'Tropical Depression detected';
	} else if($windSpeed >= 62 && $windSpeed >= 70){
		return 'blala';
	} else {
		return 'no no';
	}
};


//Conditional
function isUnderAge($age){
	return ($age < 18) ? 'legal age' : 'under age';
};


//Switch Statement
function determineComputer($computerNumber){
	switch ($computerNumber) {
		case 1:
			return 'Linus Torvalds';
			break;
		case 2:
			return 'Steve Jobs';
			break;
		case 3:
			return 'Sid Meier';
			break;
		case 4:
			return'Onel De Guzman';
			break;
		case 5:
			return 'Christian Salvador';
			break;
		default:
			return $computerNumber . ' is out of bounds';
			break;
	}
};


function greeting($str){
	try{
		if(gettype($str) === "string"){
			echo $str;
		}
		else{
			throw new Exception("Oops!");
		}
	}
	catch (Exception $e){
		echo $e->getMessage();
	}
	finally{
		echo " I did it again!";
	}
}






